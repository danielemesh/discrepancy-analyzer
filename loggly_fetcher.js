"use strict";

const loggly        = require("loggly");
const moment        = require("moment");
const logglyToken   = process.env.LOGGLY_TOKEN || "";
const logglyOptions = {
	token    : logglyToken,
	subdomain: "dauup",
	// tags      : ["fake-server", `environment-${env}`],
	tags     : ["tracking-prod"],
	json     : true,
	auth     : {
		username: "dauup",
		password: "D@uUp123"
	}
};

const START  = 60;
const PERIOD = 2; // 2 minutes;
const QUERY  = {q: "*", size: 2000};
const FORMAT = "YYYY-MM-DD HH:mm:ss.SSS";

class LogglyFetcher {
	constructor() {
		this.client = loggly.createClient(logglyOptions);
	}

	getData() {
		const periods = this.getPeriods();
		console.log(periods);
		const calls = [];


		periods.forEach((period, index) => {
			const options = Object.assign({}, QUERY, periods[0]);

			calls.push(this.clientSearchAsync(options, 0 * 1000));
		});

		return Promise.all(calls);
	}

	getPeriods() {
		const end = moment().startOf("hour");
		let start = moment(end).subtract(START, "minutes");

		const periods = [];

		while (start.isBefore(end)) {

			let next = moment(start).add(PERIOD, "minutes");

			periods.push({from: start.format(FORMAT), until: next > end ? end.format(FORMAT) : next.format(FORMAT)});
			start = next;
		}

		return periods;
	}

	clientSearchAsync(options, delay = 0) {
		return new Promise((resolve, reject) => {

			setTimeout(function () {
				this.client.search(options)
						.run(function (err, results) {
							if (err)
								reject(err);
							resolve(results);
						});
			}.bind(this), delay)
		});
	};
}

module.exports = LogglyFetcher;