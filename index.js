"use strict";

const LogglyFetcher = require("./loggly_fetcher");

const express = require("express");

const port = process.env.FAKE_SERVER_PORT || 4005;

let app = express();

app.set("view engine", "pug");

app.get("/query", (req, res) => {

	const fetcher = new LogglyFetcher();

	fetcher.getData().then(data => {
		res.json(data);
		res.end();
	});
});

app.listen(port, () => {
	console.log(`app listening on port ${port}`);
});